<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'impactpro.ro');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<RQov!uIIW(x1LLpwXotj;O+iP7]+s lSLtC*+pm+e^[YsNU_) gGkU(N-{GnNc^');
define('SECURE_AUTH_KEY',  'H5OYV;9cpAs?dU0XcJ:kIvRdd9LrY `VbMI8 SJ9oz[HY? 9~ +NU0F!| @IU}O?');
define('LOGGED_IN_KEY',    'KDCJ$W3=S+F-MqW(rV+|S>$;>z+ MDu6A6CrNYYjfld wsyDrbw-bbUYY;-5+=Ud');
define('NONCE_KEY',        '!y mj38(|`-<SC=h}-z, 5h.WJ!}an}HzY*-$=Q)jDpe+b,AaXtv>y`[R:!Q=O{+');
define('AUTH_SALT',        ')Hs;ksmwTk)P@m}Zt)g dUGUQPF[Bt!-F@G:Qw|0|(`52ru:&9Aovw5=| trv|K>');
define('SECURE_AUTH_SALT', '_:]&V>}ey$+K~fl/|3{/dX1_l4kBPRj?lY>c4zZxO!p%y1L`-nGKLVdziX%*J<oE');
define('LOGGED_IN_SALT',   'caQOq$_&X^?m%2hFD,(KvheA:m/R%c0KDWWOIj>4<M5Xv,fRG06]|[9lkHS|&g=J');
define('NONCE_SALT',       '^2[>%@&PUv#(Nxq$w<t4ZOT,sb)5s) iBKD*vzHMs,JKVnOK_J/@^F$]@YW]LpEz');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ipro66_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_ENV', 'development');
define ('WP_POST_REVISIONS', 10);
define('AUTOSAVE_INTERVAL', 180);
// Empty trash interval (number of days)
define('EMPTY_TRASH_DAYS', 30); // empty monthly

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
