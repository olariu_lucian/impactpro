<section class="section section-white">
    <?php
    $args = array(
        'orderby'           => 'count',
        'order'             => 'ASC',
        'hide_empty'        => true
    );
    $terms = get_terms('project_type', $args);
    ?>
    <div class="container">
        <div class="row">
            <div class="butoane-wrap text-center">
                <div class="button-group filter-button-group">
                    <span class="tipuri-proiecte">Tipuri proiecte - </span>
                    <button data-filter="*" class="btn button btn-default">Toate</button>
                    <?php	foreach ($terms as $term){  ?>
                        <button data-filter=".<?php echo $term->slug ?>" class="btn button btn-default project-type"><?php echo $term->name ?></button>
                    <?php  } ?>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <?php $portofoliu = new WP_Query (array('post_type'=>'portofoliu', 'posts_per_page' => -1, 'post_status' => 'publish')); ?>
    <div class="grid row clearfix">
        <?php while ($portofoliu->have_posts()) : $portofoliu->the_post(); ?>
            <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php endwhile; ?>
    </div>

</section>

