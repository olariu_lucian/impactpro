<?php //get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
	<div class="alert alert-warning margin-top">
		<?php _e('Sorry, no results were found.', 'sage'); ?>
	</div>
	<?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
	<?php //get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php get_template_part('templates/content','search'); ?>
<?php endwhile; ?>
<div class="row">
<?php
global $wp_query;

$big = 999999999; // need an unlikely integer
$translated = __( 'Page', 'sage' ); // Supply translatable string
echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '/page/%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'prev_text' => __('< Previous'),
	'next_text'	=> __('Next >'),
	'type'	=> 'list'
) );
?>
</div>