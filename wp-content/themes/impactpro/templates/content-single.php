<?php while (have_posts()) : the_post(); ?>
<article <?php post_class(); ?>>
    <?php if (has_post_thumbnail()) {
                the_post_thumbnail('post-index', array('class' => 'img-responsive') );
                } ?>
      <?php get_template_part('templates/entry-meta'); ?>        
    <div class="entry-content">
      <?php the_content(); ?>
    </div>

    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>

</article>
<?php endwhile; ?>