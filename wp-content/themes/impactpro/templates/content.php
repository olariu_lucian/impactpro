<?php 
$posts_term = wp_get_object_terms(get_the_ID(),'category');
$posts_tags = wp_get_object_terms(get_the_ID(),'post_tag');
?>

<div class="col-xs-12 no-padding categories-tags <?php foreach ($posts_term as $post_term) {echo ' '.$post_term->name; } foreach ($posts_tags as $post_tag) {echo ' '.$post_tag->name; } ?>">
    <div class="thumbnail">
      <div class="entry-content-wrapper">
          <?php get_template_part('templates/entry-meta'); ?>
      </div>
    	<a href="<?php echo the_permalink();?>">
          <?php if ( has_post_thumbnail() ) {
            the_post_thumbnail('post-index', array('class' => 'img-responsive') );
            } ?>
        </a>         
      <div class="caption">
        <h3><a href="<?php echo the_permalink();?>"><?php the_title(); ?></a></h3>
        <?php the_excerpt(); ?>
        
      </div>
    </div>
  </div>