<?php use Roots\Sage\Titles;

if(is_page(36) || is_page(406) || is_post_type_archive('portofoliu')) { ?>
    <div class="container breadnotitle">
        <?php if ( function_exists('yoast_breadcrumb') )
        {yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumbs">','</p>');} ?>
    </div>
<?php } else {
    global $post;
    $state = get_post_meta($post->ID, 'bg_on_off', true);
    $bg = get_post_meta($post->ID, 'page_header_bg', false);
    $bg = $bg[0];
    ?>
    <section id="sep-title" class="" style="background-image:url(<?php if($bg['background-image']){echo $bg['background-image'] ; }else{} ?>) ;
        background-repeat:<?php if($bg['background-repeat']){echo $bg['background-repeat'] ; }else{ echo 'repeat-x';} ?>;
        background-attachment:<?php if($bg['background-attachment']){echo $bg['background-attachment'] ; }else{ echo 'fixed';} ?>;
        background-position:<?php if($bg['background-position']){echo $bg['background-position'] ; }else{ echo 'top';} ?>;
        background-size: <?php if($bg['background-size']) {echo $bg['background-size'];} else {echo 'cover';}?>">
        <div class="container">
            <?php if ( function_exists('yoast_breadcrumb') )
            {yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumbs">','</p>');} ?>
        </div>
        <div class="page-header">
            <h1>
                <?= Titles\title(); ?>
            </h1>

        </div>

    </section>
<?php }
