
<div class="row margin-top">    
      

    <div class="col-sm-12 col-md-12 no-padding">
        <div class="thumbnail clearfix">

            <div class="caption  no-padding">
                <div class="<?php if(has_post_thumbnail()) {echo 'col-md-8 col-sm-8 ';} else {echo 'col-md-12';} ?> no-padding">
                    <h3><a href="<?php echo the_permalink();?>"><?php the_title(); ?></a></h3>
                    <p><?php the_excerpt(); ?></p>
                </div>
            </div>

                    <?php if ( has_post_thumbnail() ) { ?>
                            <div class="col-md-4 col-sm-4 ">
                             <?php the_post_thumbnail('thumbnail' , array('class' => 'img-responsive center-block margin-top') ); ?>
                            </div>
                   <?php } ?>
          
        </div>
   </div>
        
    
      
        
    

</div><!-- row end -->
