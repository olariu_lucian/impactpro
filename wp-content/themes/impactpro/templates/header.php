<?php use Roots\Sage\Nav;
$logo = ot_get_option('header_logo',false);
$contact_details = ot_get_option('contact_details', array());
$social_icons = ot_get_option('social_icons', array());
?>
<div class="topbar">
  <div class="container">
    
    
    <div class="pull-left address-container" >
      
      <ul class="list-inline list-unstyled topbar-list-address">
      <?php
        foreach($contact_details as $contact) {
          if(!empty($contact['link'])) {
            echo '<li><a href="'.$contact['link'].'" ><span class="'.$contact['icon_class'].'"></span>&nbsp;'.$contact['text'].'</a></li>';
          } else {
            echo '<li><span class="'.$contact['icon_class'].'"></span>&nbsp;'.$contact['text'].'</li>';
          }

        }
      ?>
      </ul>
    
     </div>
     
     <div class="pull-right social-container">
        <ul class="social-icons list-inline topbar-list-scoial-icons">
          <?php
            foreach($social_icons as $social) { ?>
                <li> <a target="_blank" href="<?php echo $social['link'];?>"><i class="fa topbar-icon <?php echo $social['icon_class'];?>"></i></a></li>
           <?php }?>
        </ul>
      </div>
  
</div>
</div>
<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="<?php esc_url(home_url('/')); ?>"><img
            class="img-responsive" src="<?php echo $logo;?>" alt="<?php bloginfo('name'); ?>"/> </a>
    </div>
    
    <nav class="collapse navbar-collapse navbar-right" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new Nav\SageNavWalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
    </nav>

  </div>
</header>