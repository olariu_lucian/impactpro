<?php 
$posts_term = wp_get_object_terms(get_the_ID(),'project_type');
$state = get_post_meta($post->ID, 'gallery_on_off', true);
//echo '<pre>', var_dump($posts_term), '</pre>';
if($state == 'off') {
    $attachment_id = get_post_thumbnail_id( $post->ID );
    $image_attributes = wp_get_attachment_image_src( $attachment_id,'full',false );
    $link = $image_attributes[0];
} else {
    $link = get_the_permalink();
}
?>

<div class="col-xs-12 col-sm-4 col-md-3  grid-item element-item <?php foreach ($posts_term as $post_term) {echo ' '.$post_term->slug; } ?>">
        <div class="recent-work-wrap">
            <a href="<?php echo $link;?>">
            <?php if ( has_post_thumbnail() ) {
    				the_post_thumbnail('portofoliu', array('class' => 'img-responsive portofoliu-img') );
    				} ?>
            <div class="overlay-proiecte-recente">
                <div class="recent-work-inner">
                    <h3><?php  the_title(); ?></h3>
                    <p><?php  the_excerpt(); ?></p>
                    <i class="fa fa-search"></i>
                </div> 
            </div>
            </a>
        </div>

</div>
