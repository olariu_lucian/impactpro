<footer id="footer" class="clearfix" role="contentinfo" style="padding-bottom:0px;">
  <?php
  $footer_copyright_text = ot_get_option('footer_copyright_text',false);
  $social_icons_bottom = ot_get_option('social_icons', array());
  ?>
    <div class="content-info clearfix">
    <div class="container">
      <div class="footer-widgets">
        <div class="row">
          <?php dynamic_sidebar('sidebar-footer'); ?>
        </div>
      </div>
    </div>
      <div class="copyright">
        <div class="container">
          <div class="row">
          <div class="col-xs-12">
            <p class="copyright-text pull-left hidden-xs">
              <?php echo $footer_copyright_text; ?>
            </p>
            <span class="copyright-symbol pull-left visible-xs">&copy;</span>
            <ul class="footer-social list-inline list-unstyled pull-right">
              <?php foreach ($social_icons_bottom as $icon) { ?>
                <li> <a target="_blank" href="<?php echo $icon['link'];?>"><i class="fa <?php echo $icon['icon_class'];?>"></i></a></li>

              <?php } ?>

            </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
</footer>