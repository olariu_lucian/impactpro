<?php
global $post;
$post_id = $post->ID;
$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
$terms = wp_get_post_terms( $post_id, 'project_type', $args );

while (have_posts()) : the_post();
  // Getting post metabox values
  $state = get_post_meta($post->ID, 'gallery_on_off', true);
  $gallery = get_post_meta($post->ID, 'portofoliu_gallery', true);
  $p_ids = explode(",", $gallery);
  
?>
<section <?php post_class('col-md-8'); ?>>
    <?php $link = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
    <?php //echo '<pre>',var_dump($link),'</pre>';?>
    <div class="row">
        <div class="gallery">
            <div class="item-types">
              <?php foreach ($terms as $term) { ?>
              <li class="item-types-li"><?php echo  $term->name; ?></li>
              <?php } ?>
            </div>
            <a href="<?php echo $link[0];?>">
              <?php if (has_post_thumbnail()) {
                the_post_thumbnail('post-index', array('class' => 'img-responsive portofoliu-img single-portofoliu-img no-padding') );
              } ?>
            </a>
        </div>
    <?php
// Check to see if state is on or off and $p_ids array not empty.
    if ($state == 'on' and !empty($p_ids)) { ?>
        <h3><?php _e('A gallery of the portfolio item'); ?></h3>
        <hr style="width:250px; margin-left:0px; floate:left;">
         <div class="gallery">
            <?php foreach ($p_ids as $gal_pic_id) {
                $link = wp_get_attachment_image_src($gal_pic_id, 'full');
                $image = wp_get_attachment_image($gal_pic_id, 'thumbnail', array('class' => 'img-responsive single-gallery')); ?>
                <div class="col-md-3 col-sm-3 col-xs-4 no-padding">
                  <a href="<?php echo $link[0];?>">
                  <?php echo $image; ?>
                  </a>
                </div>
            <?php } ?>
          </div>
        <?php } ?>
    </div> <!-- row end end -->
</section>
    <div class="entry-content col-md-4">
      <div class="row">
      <?php the_content(); ?>
      </div>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php// comments_template('/templates/comments.php'); ?>
<?php endwhile; ?>