/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        //Wowjs Init\
        $(window).load(function(){

          new WOW().init();

        });

        // if ($(window).width()>992 ) {
        $(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
        // }


        
      //-------------------- Adding some classes to various elements around the DOM --------------------------

      $('.page-numbers').addClass('pagination'); // Pagination nav
      $('.gallery > div > a > img').addClass('img-responsive single-gallery');

        //-------------------------- Footer bottom----------------------------
        $(document).ready(function(){
          $('.banner').scrollToFixed();
          function FooterBottom() {
          var docHeight = $(window).height();
          var footerHeight = $('#footer').height();
          var footerTop = $('#footer').position().top + footerHeight;

          if (footerTop < docHeight) {
            $('#footer').css('margin-top', (docHeight - footerTop) + 0 +  'px');
          }
        }

           $(window).resize(function() {
            new FooterBottom();
          });
          new FooterBottom();
        });
        

        //Topbar Script --------------------------------------------------------
        function Topbar(){
        var window_width = window.innerWidth;
          if (window_width > 992 ) {
              $('.topbar-list-address').addClass('pull-left');
              $('.topbar-list-scoial-icons').addClass('pull-right');
            }

            $('.topbar-list-address').css('margin', '0px');
            $('.topbar-list-scoial-icons').css('margin','0px');
        }
        $(window).resize(function() {
            new Topbar();
          });
          new Topbar();

        //Swiper slider Homepage
        $(document).ready(function () {
          var swiper = new Swiper('.s0', {
            loop: true,
            effect:'cube',
            pagination: '.swiper-pagination',
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 30,
            autoplay: 4500,
            autoplayDisableOnInteraction: true
          });
          var swiper1 = new Swiper('.s1', {
            loop: true,
            effect:'slide',
            direction:'horizontal',
            pagination: '.swiper-pagination',
            paginationClickable: true,
            nextButton: '.next-btn',
            prevButton: '.prev-btn',
            autoplay: 4500,
            autoplayDisableOnInteraction: true,
            grabCursor: true
          });

       //Init magnificent popup
          $('.image-link, a[href$="jpg"], a[href$="png"], a[href$="jpeg"]').magnificPopup({type:'image'});
          $('.gallery').each(function() { // the containers for all your galleries
              $(this).magnificPopup({
                  delegate: 'a', // the selector for gallery item
                  type: 'image',
                  gallery: {
                    enabled:true
                  }
              });
          });
        });
        
        //Banner scroll to to fixed
        //$('.banner').scrollToFixed({
        //  preFixed: function() { $(this).find('h1').css('color', 'blue'); },
        //  postFixed: function() { $(this).find('h1').css('color', ''); }
        //});
        $(".placehold label").each(function() {
          var label = $(this);
          var placeholder = label.text();
          label.closest(".gfield").find("input").attr("placeholder", placeholder).val("").focus().blur();
        });
        
        // Collapsed and opened dropdown menu backgroud fix
        function NavbarCollpaseBackground() {
          var nav = $('.navbar-collapse');
          var scroll = window.scrollY;
            if (scroll >30 ) {
              nav.css({'background-color':'#f0f0f0', 'margin-top':'0px'});
            }
            

            $( window ).resize(function() {
                $(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
            });

           
        }


        $(window).scroll(function() {
            new NavbarCollpaseBackground();
          });
          new NavbarCollpaseBackground();

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'blog': {
      init: function() {
        // JavaScript to be fired on the home page

        // init Isotope
      $(window).load(function(){

          var $grid = $('.terms').isotope({
          layoutMode: 'fitRows',
          itemSelector: '.categories-tags'
          
        });

      });  

      // store filter for each group
      var filters = {};

      $('.filters').on( 'click', '.button', function() {
        var $this = $(this);
        // get group key
        var $buttonGroup = $this.parents('.button-group');
        var filterGroup = $buttonGroup.attr('data-filter-group');
        // set filter for group
        filters[ filterGroup ] = $this.attr('data-filter');
        // combine filters
        var filterValue = concatValues( filters );
        // set filter for Isotope
        $grid.isotope({ filter: filterValue });
      });

      // change is-checked class on buttons
      $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
          $buttonGroup.find('.is-checked').removeClass('is-checked');
          $( this ).addClass('is-checked');
        });
      });
  


      // flatten object by concatting values
      function concatValues( obj ) {
        var value = '';
        for ( var prop in obj ) {
          value += obj[ prop ];
        }
        return value;
      }
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },

      // Home page
    'contact': {
      init: function() {
        // JavaScript to be fired on the home page

        // Maps Responsive
         $('iframe[src*="google.com"]').wrap('<div class="map-container"/>');
    
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'post_type_archive_portofoliu': {
      init: function() {
        // JavaScript to be fired on the portofoliu page
        // ----------------------------------init Isotope Portfolio-------------------------------------------------------
        $(window).load(function(){
          var $container = jQuery('.grid').isotope({
          // options
          itemSelector: '.grid-item'
        });
        // filter items on button click
        jQuery('.filter-button-group').on( 'click', 'button', function() {
          var filterValue = jQuery(this).attr('data-filter');
          $container.isotope({ filter: filterValue });
        });

        });
        

        //----------------------------------------------------------------------------------------------------------------

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
