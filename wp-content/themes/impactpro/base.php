<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>
<?php if(is_page(62)) {

  // redirect pagina servicii -> dezvoltare site web
  wp_redirect (get_permalink(12),302);
  exit;
}?>
<?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
  <?php include_once("templates/analyticstracking.php"); ?>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    if(!is_front_page()) {
      get_template_part('templates/page', 'header');
    }
    ?>
    <?php if ( is_active_sidebar( 'sidebar-primary' ) && !is_front_page() && !is_page_template('template-contact.php')) {
      $container_class = 'container';
      //$container_class = '';
      $section_class = '';
    } else {
      $container_class = '';
      $section_class = '';
    } ?>
    <section class="wrap <?php echo $section_class; ?> clearfix" role="document">
      <div class="content <?php echo $container_class; ?>">
        <main class="main" role="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Config\display_sidebar()) : ?>
          <aside class="sidebar" role="complementary">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </section><!-- /.wrap -->
    <?php
      get_template_part('templates/footer');
      wp_footer();
    ?>
    
  </body>
  
</html>
