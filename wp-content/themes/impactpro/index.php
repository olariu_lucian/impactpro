<?php //get_template_part('templates/page', 'header'); ?>

<?php if ( is_home() ) { ?>
<?php

$args = array(
    'orderby'           => 'count', 
    'order'             => 'ASC',
    'hide_empty'        => true,  
); 

$terms = get_terms('category', $args);
?>
<div class="filters">		
		<section class=" categories">
			<h3><?php echo __('Post Categories', 'sage'); ?></h3>
				<div class="butoane-wrap">
					
						<div class="button-group filter-button-group" data-filter-group="category">
			 						
			 					<button data-filter="*" class="btn button btn-default button">Toate</button>

								<?php	foreach ($terms as $term){ ?>
				
									<button data-filter=".<?php echo $term->name ?>" class="btn btn-default button project-type"><?php echo $term->name ?></button> 

								<?php } ?>
						</div>
					
				</div>
		</section>


</div> <!-- end filters -->
<hr>	
<?php } ?>


<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>



<div class="post-wrapper">
	<div class="terms">
	
	<?php while (have_posts()) : the_post(); ?>
	  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>
		
	</div>
</div>

<nav class="pages">
<?php
global $wp_query;

$big = 999999999; // need an unlikely integer
$translated = __( 'Page', 'sage' ); // Supply translatable string
echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '/page/%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages,
	'prev_text' => __('< Previous','sage'),
	'next_text'	=> __('Next >','sage'),
	'type'	=> 'list'
) );
?>
</nav>
