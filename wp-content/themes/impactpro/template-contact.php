<?php
/**
 * Template Name: Contact Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <section class="section-light">
        <div class="container">
            <?php get_template_part('templates/content', 'page'); ?>
        </div>
    </section>
    

<?php endwhile; ?>
