<?php
class impact_portfolio extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'Impact Portfolio' );
	}

	function widget( $args, $instance ) {
		// Widget output
		?>
		<div class="widget">
			<h3>Proiecte Recente</h3>
		</div>
		<div class="media">
  			<div class="media-left">
    			<a href="#">
      				<img class="media-object" src="..." alt="...">
    			</a>
  			</div>
  		<div class="media-body">
    		<h4 class="media-heading">Media heading</h4>
		    ...
		</div>
		</div>
		<?php
		$query = new WP_query(array('post_type'=>'portofoliu', 'post_per_page'=>3, 'orderby'=>'date'));
		//echo '<pre>',var_dump($query),'</pre>';
		if ( have_posts() ) {
			while ( $query->have_posts() ) {
					$query->the_post(); 
					the_title();
	} // end while
} // end if

	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
	}

	function form( $instance ) {
		// Output admin widget options form
	}
}

function impact_portfolio() {
	register_widget( 'impact_portfolio' );
}

add_action( 'widgets_init', 'impact_portfolio' );
