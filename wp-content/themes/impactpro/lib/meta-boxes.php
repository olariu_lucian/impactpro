<?php
// This file contains general metabox options. The CPT metaboxes are in the cpt-*name.php files, below the registration of CPT.


// Sedi Metabox
add_action( 'admin_init', 'general_meta_boxes' );
add_action('admin_init', 'post_gallery');
add_action('admin_init', 'title_bg');

function general_meta_boxes() {
    $general_meta_box = array(
        'id'          => 'general_meta',
        'title'       => __( 'Page Settings', 'sage' ),
        'desc'        => '',
        'pages'       => array( 'page' ),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      => array(
            array(
                'id'          => 'sidebar_on_off',
                'label'       => __( 'Display Sidebar on this page', 'theme-text-domain' ),
                'desc'        => '',
                'std'         => 'off',
                'type'        => 'on-off',
                'section'     => 'option_types',
                'operator'    => 'and'
            ),


        )
    );

    /**
     * Register our meta boxes using the
     * ot_register_meta_box() function.
     */
    if ( function_exists( 'ot_register_meta_box' ) )
        ot_register_meta_box( $general_meta_box );



}

function post_gallery() {
    $post_gallery = array(
        'id'          => 'gallery_meta',
        'title'       => __( 'Gallery', 'sage' ),
        'desc'        => '',
        'pages'       => array( 'portofoliu' ),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      => array(
           array(
                'id'          => 'gallery_on_off',
                'label'       => __( 'Display a gallery on this page', 'theme-text-domain' ),
                'desc'        => '',
                'std'         => 'off',
                'type'        => 'on-off',
                'section'     => 'option_types',
                'operator'    => 'and'
            ),

           array(
                'id'          => 'portofoliu_gallery',
                'label'       => __( 'Choose images', 'theme-text-domain' ),
                'desc'        => __( 'The Gallery option type saves a comma separated list of image attachment IDs. You will need to create a front-end function to display the images in your theme.', 'theme-text-domain' ),
                'std'         => '',
                'type'        => 'gallery',
                'section'     => 'option_types',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'min_max_step'=> '',
                'class'       => '',
                'condition'   => 'gallery_on_off:is(on)',
                'operator'    => 'and'
              ),
           


        )
    );

    /**
     * Register our meta boxes using the
     * ot_register_meta_box() function.
     */
    if ( function_exists( 'ot_register_meta_box' ) )
        ot_register_meta_box( $post_gallery );



}

function title_bg() {
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */
  $title_bg = array(
    'id'          => 'title_bg',
    'title'       => __( 'Title Background Image', 'Sage' ),
    'desc'        => '',
    'pages'       => array( 'page','post','portofoliu' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
        array(
        'id'          => 'bg_on_off',
        'label'       => __( 'On/Off', 'theme-text-domain' ),
        'desc'        => __( 'Turn page header background on or off', 'Sage' ),
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),

         array(
        'id'          => 'page_header_bg',
        'label'       => __( 'Background', 'Sage' ),
        'desc'        => sprintf( __( 'The Background option type is for adding background styles to your theme either dynamically via the CSS option type below or manually with %s. The Background option type has filters that allow you to remove fields or change the defaults. For example, you can filter %s to remove unwanted fields from all Background options or an individual one. You can also filter %s. These filters allow you to fine tune the select lists for your specific needs.', 'theme-text-domain' ), '<code>ot_get_option()</code>', '<code>ot_recognized_background_fields</code>', '<code>ot_recognized_background_repeat</code>, <code>ot_recognized_background_attachment</code>, <code>ot_recognized_background_position</code>, ' . __( 'and', 'theme-text-domain' ) . ' <code>ot_type_background_size_choices</code>' ),
        'std'         => '',
        'type'        => 'background',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'bg_on_off:is(on)',
        'operator'    => 'and'
      ),
    )
  );
  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
  if ( function_exists( 'ot_register_meta_box' ) )
    ot_register_meta_box( $title_bg );

}


