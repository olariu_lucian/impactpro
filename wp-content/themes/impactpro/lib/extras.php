<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/************* ADD Featured Image column and thumbnails *****************/
// Add the posts and pages columns filter. They can both use the same function.
add_filter('manage_posts_columns',  __NAMESPACE__ . '\\ateliermore_add_post_thumbnail_column', 2);
add_filter('manage_pages_columns',  __NAMESPACE__ . '\\ateliermore_add_post_thumbnail_column', 2);

// Add the column
function ateliermore_add_post_thumbnail_column($cols){
  $cols['ateliermore_post_thumb'] = __('Featured Image','sage');
  return $cols;
}

// Hook into the posts an pages column managing. Sharing function callback again.
add_action('manage_posts_custom_column', __NAMESPACE__ . '\\ateliermore_display_post_thumbnail_column', 2, 2);
add_action('manage_pages_custom_column', __NAMESPACE__ . '\\ateliermore_display_post_thumbnail_column', 2, 2);

// Grab featured-thumbnail size post thumbnail and display it.
function ateliermore_display_post_thumbnail_column($col, $id){
  switch($col){
    case 'ateliermore_post_thumb':
      if( function_exists('the_post_thumbnail') )
        echo the_post_thumbnail( array(70,70) );
      else
        echo 'Not supported in theme';
      break;
  }
}
/************* login logo url change *****************/
function change_wp_login_url() {
  return home_url();
}
add_filter('login_headerurl', __NAMESPACE__ . '\\change_wp_login_url');

/************* login logo title change *****************/

function change_wp_login_title() {
  return get_option('blogname');
}

add_filter('login_headertitle', __NAMESPACE__ . '\\change_wp_login_title');

/**
 * Admin footer text
 */

function remove_footer_admin () {
  echo 'Created by ImpactPro.ro';
}
add_filter('admin_footer_text', __NAMESPACE__ . '\\remove_footer_admin');

/**
 * Hide wp icon and links from the admin bar
 */
function annointed_admin_bar_remove() {
  global $wp_admin_bar;
  /* Remove their stuff */
  $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', __NAMESPACE__ . '\\annointed_admin_bar_remove', 0);

/**
 * Admin Login logo
 */
function my_login_logo() {
  //$admin_logo = ot_get_option('home_logo',true);
  $upload_dir = wp_upload_dir();
  $admin_logo = $upload_dir['baseurl'].'/logo.png'
  ?>
  <style type="text/css">
    body.login {background:#30282a}
    body.login .message {color:#FFF;background: #4b4043;}
    .login input[type="text"], .login input[type="password"] {font-size:16px; border-radius:5px;padding:6px;}
    body.login #loginform {background: #4b4043; box-shadow: 1px 1px 10px #111;}
    body.login div#login h1 a {background-image: url( <?php echo $admin_logo; ?>); background-size: 100%;  padding-bottom: 0; width:138px; height:136px;}
    #login {padding-top:30px;}

  </style>
<?php }
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo' );

/**
 * Gravity Forms anchor - disable auto scrolling of forms
 */
add_filter('gform_confirmation_anchor', '__return_false');

/**
 * Add extra image sizes in WP-admin
 */
function my_insert_custom_image_sizes( $sizes ) {
  global $_wp_additional_image_sizes;
  if ( empty($_wp_additional_image_sizes) )
    return $sizes;

  foreach ( $_wp_additional_image_sizes as $id => $data ) {
    if ( !isset($sizes[$id]) )
      $sizes[$id] = ucfirst( str_replace( '-', ' ', $id ) );
  }

  return $sizes;
}
add_filter( 'image_size_names_choose', __NAMESPACE__ . '\\my_insert_custom_image_sizes' );

/**
 * Add automatic image class to editor
 */
function add_image_responsive_class($content) {
  global $post;
  $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
  $replacement = '<img$1class="$2 img-responsive"$3>';
  $content = preg_replace($pattern, $replacement, $content);
  return $content;
}
add_filter('the_content',  __NAMESPACE__ . '\\add_image_responsive_class');

/**
 * Attach a class to linked images' parent anchors
 * Works for existing content
 */
function give_linked_images_class($content) {
  $classes = 'fancybox'; // separate classes by spaces - 'img image-link'
  // check if there are already a class property assigned to the anchor
  if ( preg_match('/<a.*? class=".*?"><img/', $content) ) {
    // If there is, simply add the class
    $content = preg_replace('/(<a.*? class=".*?)(".*?><img)/', '$1 ' . $classes . '$2', $content);
  } else {
    // If there is not an existing class, create a class property
    $content = preg_replace('/(<a.*?)><img/', '$1 class="' . $classes . '" ><img', $content);
  }
  return $content;
}

// add searchform to nav menu
add_filter('wp_nav_menu_items', __NAMESPACE__ . '\\add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {

 if( $args->theme_location == 'primary_navigation' ) :
  $items .= '<li class="dropdown menu-item menu-icon menu-search"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
            <ul class="dropdown-menu" style="padding:8px;">
                <form role="search" method="get" class="search-form form-inline" action="'.esc_url(home_url('/')).'">
  <label class="sr-only">'. __('Search for:', 'sage').'</label>
  <div class="input-group">
    <input type="search" value="'.get_search_query().'" name="s" class="navbar-search search-field form-control input-sm" placeholder="'. __('Search', 'sage').'" required>
    <span class="input-group-btn">
      <button type="submit" class="search-submit btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
    </span>
  </div>
</form>
              </ul></li>';  
 endif;
 return $items;

}

// Filter for the about us page to change base.php layout (no container) ---------------------------------------------------------------------------------------  

add_filter('sage/wrap_base', __NAMESPACE__ . '\about_template_wrapper'); // Add our function to the sage/wrap_base filter

function about_template_wrapper($templates) {
  $pageTemplate = get_page_template();

  $pageArray = explode("/", $pageTemplate);

  $pageTemplate = end($pageArray);

  if ($pageTemplate == 'template-despre.php' ) {
     $name = 'nocontainer';
     if (!is_search()) {
     array_unshift($templates, 'base-' . $name . '.php'); // Shift the template to the front of the array
    }
  }
  return $templates; // Return our modified array with base-$cpt.php at the front of the queue
}