<?php
/**
* CPT Portfolio
 */



function register_portfolio_type()
    {
        $xenomorph = array(
            'labels'	=>	array(
                'name'				=>		__('Portfolio', 'sage'),
                'singular_name' 	=> 		__('Portfolio', 'sage'),
                'add_new' 			=>		__('Add new portfolio item', 'sage'),
                'add_new_item'		=>		__('Add new portfolio item', 'sage'),
                'edit_item'			=>		__('Edit portfolio item', 'sage'),
                'new_item'			=>		__('New portfolio item', 'sage'),
                'view_item'			=>		__('View portfolio item', 'sage'),
                'search_items'		=>		__('Search portfolio items', 'sage'),
                'not_found'			=>		__('No portfolio items found', 'sage'),
                'not_found_in_trash'=>		__('No portfolio items found in trash', 'sage'),
                'name_admin_bar' 	=>		__('Portfolio')
            ),
            'query_var' 			=> true,
            'delete_with_user' 		=> false,
            'rewrite'		=>	array(

                'slug'				=>		__('portofoliu')

            ),
            'public'		=> true,
            'exclude_from_search' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-screenoptions',
            'has_archive' 			=> true,
            'supports'		=> array(

                'title',
                'editor',
                'thumbnail',
                'excerpt',
                'post-formats',
                'page-attributes'

            ),
        );

        register_post_type('portofoliu', $xenomorph);
    };
add_action( 'init', 'register_portfolio_type' );


function register_portfolio_taxonomy()
    {
        $labels = array(
            'name'                       => __( 'Types', 'sage' ),
            'singular_name'              => __( 'Type', 'sage' ),
            'search_items'               => __( 'Search Types', 'sage' ),
            'popular_items'              => __( 'Popular Types', 'sage' ),
            'all_items'                  => __( 'All Types', 'sage' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Type', 'sage' ),
            'update_item'                => __( 'Update Type', 'sage' ),
            'add_new_item'               => __( 'Add New Type', 'sage' ),
            'new_item_name'              => __( 'New Type' ),
            'separate_items_with_commas' => __( 'Separate types with commas', 'sage' ),
            'add_or_remove_items'        => __( 'Add or remove types', 'sage' ),
            'choose_from_most_used'      => __( 'Choose from the most used types', 'sage' ),
            'not_found'                  => __( 'No types found.', 'sage' ),
            'menu_name'                  => __( 'Types', 'sage' ),




        );


        $xenomorph = array(
            'hierarchical'			=> true,
            'labels'				=> $labels,
            'show_ui'				=> true,
            'show_admin_colum'		=> true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'				=> true,
            'rewrite'				=> array('slug' => 'project-type'),
            'description'			=> __('Enter the type for the portfolio item', 'sage'),



        );

        register_taxonomy('project_type', 'portofoliu', $xenomorph);
    };
add_action( 'init', 'register_portfolio_taxonomy', 0 );