<?php //get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'sage'); ?>
    </div>
    <?php get_search_form(); ?>
<?php endif; ?>
<section class="sec-home separator">
    <div class="container">
        <div class="swiper-container s0">
            <div class="swiper-wrapper">
               <?php if ( function_exists( 'ot_get_option' ) ) {
                   /* get the slider array */
                   $slides = ot_get_option( 'slider', array() );
                   if ( ! empty( $slides ) ) {
                       foreach( $slides as $slide ) {
                           $slide_image = wp_get_attachment_image($slide['image'],'large',0,array('class' =>
                               'img-responsive slider-img center-content'));
                           echo '<div class="swiper-slide">
                              <div class="left-side col-xs-12 col-md-6">
                                <a href="' . $slide['slide_btn_link'] . '"><h1 class="slider-title">' . $slide['title'] . '</h1></a>
                                <div class="col-xs-12"><h4 class="slider-subtitle">'.$slide['slide_subtitle'].'</h4></div>
                                <div class="col-xs-12 description">' . wpautop($slide['slide_description']) . '</div>
                                <div class="slide-btn col-xs-6 col-xs-offset-3 text-center"><a class="btn btn-sm btn-primary" href="'.$slide['slide_btn_link'].'">'.$slide['slide_btn_text'].'</a></div>
                              </div>
                              <div class="right-side col-xs-12 col-md-6 no-padding-right pull-right"><a
                              href="' . $slide['slide_btn_link'] . '">'.$slide_image.'</a></div>
                          </div>';
                       }
                   }
               }
               ?>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"><i class="fa fa-angle-right"></i> </div>
            <div class="swiper-button-prev"><i class="fa fa-angle-left"></i> </div>
        </div>
    </div>
</section>
<section id="feature" class="section section-light">
    <div class="container">
        <div class=" page-content">
        <?php while (have_posts()) : the_post(); ?>
        <div class="row">
            <div class="col-xs-12 col-sm-7">
              <div class="clearfix servicii-descriere-panel">
                   <?php the_content(); ?>
               </div>
            </div>
            <div class="col-xs-12 col-sm-5">
                <?php if ( has_post_thumbnail() ) {
                     the_post_thumbnail('portofoliu-single', array( 'class' => 'img-responsive center-content features-img wow fadeInRightBig' ));
                }
                ?>
            </div>
        </div>
        <?php endwhile; ?>
        </div>

        <div class="row">
            <div class="features">
                <?php if (function_exists('ot_get_option')) { ?>
                      <?php  $servicii = ot_get_option('services', ARRAY_A); ?>
                      <?php foreach ($servicii as $serviciu ) { ?>
                        <div class="hpservice margin-bottom col-xs-12 col-sm-6 wow <?php echo $serviciu['servicii_animation']?>" data-wow-duration="<?php echo $serviciu['servicii_animation_duration']?>ms" data-wow-delay="<?php echo $serviciu['servicii_animation_delay']?>ms">
                            <div class="feature-wrap col-xs-12 panel panel-default clearfix">
                                <div class="col-xs-12 no-padding text-center">
                                    <h2 class="servicii-title">
                                        <a href="<?php echo get_permalink($serviciu['servicii_page_slug']);?>"><i class="fa <?php echo $serviciu['servicii_icon']?>"></i><br/><?php echo $serviciu['servicii_title']?></a>
                                    </h2>
                                </div>
                                <div class="col-xs-12 no-padding servicii-text">
                                    <p><?php echo $serviciu['servicii_text']?></p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                    <?php  }  ?>
              <?php  }?>

            </div><!--/.services-->
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#feature-->
<section id="recent-works" class="section separator">
    <div class="container">
      <div class="row">
        <div class="text-center wow fadeInLeft">
          <?php $proiecte_recente_title = ot_get_option('proiecte_recente_title'); ?>
            <h2><a href="<?php echo get_post_type_archive_link('portofoliu');?>"><?php echo $proiecte_recente_title; ?></a></h2>
            <p class="lead"><?php if (function_exists('ot_get_option')) { echo ot_get_option('proiecte_recente');} ?></p>
        </div>
        <?php $recent_works = get_posts(
            array(
               'post_type' => 'portofoliu',
               'numberposts' => 4,
               'orderby' => 'date',
               'order' => 'DESC',
               'post_status' => 'publish'

            )
        );


        foreach ($recent_works as $recent_work) {
            $state = get_post_meta($recent_work->ID, 'gallery_on_off', true);
//echo '<pre>', var_dump($posts_term), '</pre>';
            if($state == 'off') {
                $attachment_id = get_post_thumbnail_id( $recent_work->ID );
                $image_attributes = wp_get_attachment_image_src( $attachment_id,'full',false );
                $link = $image_attributes[0];
            } else {
                $link = get_permalink($recent_work->ID);
            }
            ?>
          <div class="col-xs-6 col-sm-4 col-md-3 wow fadeInTop">
                <a href="<?php echo $link; ?>">
                <div class="recent-work-wrap">
                    <?php $proiect_img = wp_get_attachment_image_src( get_post_thumbnail_id($recent_work->ID), 'portofoliu'); ?>
                    <img src="<?php echo $proiect_img[0] ?>" class="img-responsive">
                    <div class="overlay-proiecte-recente">
                        <div class="recent-work-inner">
                            <h3><?php echo $recent_work->post_title; ?> </h3>
                            <p><?php echo $recent_work->post_excerpt; ?></p>
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>
                 </a>
            </div>
        <?php } ?>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#recent-works-->

<section id="partner" class="section section-light ">
    <div class="section-partner-overlay"></div>
    <div class="container">
        <div class=" margin-bottom">
            <div class="center wow fadeInDown">
                <?php $parteneri_title = ot_get_option('parteneri_title'); ?>
                <h2><?php echo $parteneri_title; ?></h2>
                <?php $parteneri_text = ot_get_option('parteneri_content', true);?>
                <p><?php echo $parteneri_text; ?></p>
            </div>
        </div>
        <div class="partners clearfix text-center" data-wow-duration="1000ms" data-wow-delay="300ms">
            <?php if (function_exists('ot_get_option')) {
                $partners=ot_get_option('parteneri', array());
                if (!empty($partners)){
                    foreach($partners as $partner){
                        $partner_image = wp_get_attachment_image($partner['parteneri_logo'],'parteneri',0,array('class' =>'img-responsive wow center-block fadeInDown'));
                        if(!empty($partner['parteneri_link'])) { ?>
                            <div class="col-md-2 col-sm-2 col-xs-6"> <a href="<?php echo $partner['parteneri_link'] ?>" target="_blank"><?php echo $partner_image; ?></a></div>
                        <?php } else { ?>
                            <div class="col-md-2 col-sm-2 col-xs-6"><?php echo $partner_image; ?></div>
                        <?php } ?>
                    <?php   } ?>
                <?php   } ?>
            <?php } ?>
        </div>
    </div><!--/.container-->
</section><!--/#partner-->

<section class="contact-section section section-light" >
    <?php $contact = ot_get_option('contact_text_homepage'); ?>
    <div class="container">

        <div class="row">
            <div class="text-center col-md-12 wow  fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <i class="fa fa-envelope-o contact-icon"></i>
                <?php echo $contact; ?>
            </div>

            <div class="icon text-center col-md-12 wow  fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <a href="<?php echo get_permalink(40);?>" target="_blank" class="homepage-contact-button btn btn-primary">Contactati-ne</a>
            </div>
        </div>
    </div>
</section>
<?php the_posts_navigation(); ?>